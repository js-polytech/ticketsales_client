import { Component, OnInit, OnDestroy } from '@angular/core';
import {IUser} from "../../../models/users";
import {AuthService} from "../../../services/auth/auth.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {UserService} from "../../../services/user/user.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "../../../models/erorr";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})

export class AuthorizationComponent implements OnInit,OnDestroy {

  loginText='Логин';
  pswText='Пароль';
  login: string;
  psw: string;
  selectedValue: boolean ;
  cardNumber: string;
  authTextButton: string;

  constructor (private authService: AuthService ,
               private messageService: MessageService,
               private router: Router,
               private userService: UserService,
               private http: HttpClient) { }

  ngOnInit(): void {
    // console.log('init');
    this.authTextButton="Авторизоваться";
  }

  ngOnDestroy(): void  {
    // console.log('destroy');
  }

  vipStatusSelected():void {
  }

  onAuth(ev: Event): void {

    const authUser: IUser = {
      psw: this.psw,
      login: this.login,
      cardNumber: this.cardNumber
    }

    this.http.post<{ access_token: string, id: string }>('http://localhost:3000/users/' + authUser.login, authUser).subscribe((data) => {
      authUser.id = data.id;
      this.userService.setUser(authUser);
      const token: string = data.access_token;
      this.userService.setToken(token);
      this.userService.setInStore(token);
      this.router.navigate(['tickets/tickets-list'])
    }, (err: HttpErrorResponse) => {
      console.log('err', err)
      const serverError = <ServerError>err.error;
      this.messageService.add({severity: 'warn', summary: serverError.errorText});
    });

    // if (this.authService.checkUser(authUser)){
    //   this.userService.setUser(authUser);
    //   const token: string= 'user-private-token';
    //   this.userService.setToken(token);
    //   this.userService.setInStore(token);
    // }

  }

}
