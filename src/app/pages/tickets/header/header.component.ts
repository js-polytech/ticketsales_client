import {Component, Input, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {MenuItem} from "primeng/api";
import {UserService} from "../../../services/user/user.service";
import {IUser} from "../../../models/users";
import {IMenuType} from "../../../models/menuType";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  items: MenuItem[];
  user: IUser | null;
  private settingsActive = false;

  @Input() menuType: IMenuType;
  constructor(private userService: UserService) {
  }


  ngOnInit(): void {
    this.items = [
      {
        label: 'Отели',
        routerLink: ['tickets-list']
      },
      {
        label: 'Мои заказы',
        routerLink: ['order']
      },
      {
        label: 'Контакты',
        routerLink: ['my-section']
      },
      {
        label: 'Выйти',
        routerLink: ['/auth'],
        command: (click) => {
        this.userService.removeUser()
        }
      },
    ];
    this.user = this.userService.getUser();

  }


  ngOnChanges(ev: SimpleChanges): void {
    this.settingsActive = this.menuType?.type === "extended";
    this.items = this.initMenuItems();
  }

  initMenuItems(): MenuItem[] {
    return [
      {
        label: 'Отели',
        routerLink: ['tickets-list']
      },
      {
        label: 'Мои заказы',
        routerLink: ['order']
      },
      {
        label: 'Контакты',
        routerLink: ['my-section']
      },
      {
        label: 'Настройки',
        routerLink: ['settings'],
        visible: this.settingsActive
      },
      {
        label: 'Выйти',
        routerLink: ['/auth'],
        command: (click) => {
          this.userService.removeUser()
        }
      },
    ];

  }

}

