import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MySectionComponent} from "./my-section.component";

const routes: Routes = [
  {
    path: '',
    component: MySectionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MySectionRoutingModule { }
