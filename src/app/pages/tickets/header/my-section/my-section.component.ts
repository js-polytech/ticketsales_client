import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-my-section',
  templateUrl: './my-section.component.html',
  styleUrls: ['./my-section.component.scss']
})
export class MySectionComponent implements OnInit {
  
  constructor( private router: Router) 
    { }

  ngOnInit(): void {
    this.router.navigate(['/my-section']);
  }

}
