import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MySectionRoutingModule } from './my-section-routing.module';
import {MySectionComponent} from "./my-section.component";


@NgModule({
  declarations: [
    MySectionComponent
  ],

  imports: [
    CommonModule,
    MySectionRoutingModule
  ]
})
export class MySectionModule { }
